# README #

## What is this repository for?

This project provides libraries the ability to register patrons for summer reading and to track their progress.  It also supports prize giveaways.

## How do I get set up?

### Dependencies
    
* A windows active directory user account in the clcdpc.org domain.
    * The password for this user account should be set so that it never expires
    * You should NOT reuse any other windows account for this project
* Use the create_db.sql script in the 'Extras' folder to create the database
* A SQL database user with read/write access to the Summerreading database	
    * The SQL user should be set to use Windows Authentication and you should use the active directory user account you setup in the first step
* A PAPI username and key available via the Polaris Webmin tool. PAPI is used to authentication/add patrons into the system that already have a barcode and PIN.

## Deployment instructions

* Create an HTTP IIS Website
    * This should automatically create an associated App Pool with the same name
* Edit the App Pool's identity property in advanced settings
    * Use the username and password of the user account you created earlier
	* Restart the app pool so that it is running under the new security context
* Set 'Start Mode' on the application pool to 'AlwaysRunning'
* Set 'Preload enabled' on the site to 'True'
* Adjust IIS Site to offer Negotiate as an authentication provider
* If the Summer Reading server hostname or database name have changed update the app.config and web.config file
    * Specifically check the <connectionStrings> section for any changes, you may also need to re-run the Entity Framework connection string wizard
* Adjust the PAPI authentication information in the web.config file
* Perform a standard web publish through Visual Studio

## Configuration
* Adding staff members
    * Use the following stored procedure:  ```` exec SummerReading.dbo.AddStaffMember 'domain\username', orgId, isAdmin (0/1) ````
    * **Make sure to use the org id from the SummerReading DB table.** Use the ID of the entry with a null ParentOrganizationID.



## Maintenance

* The database isn't not meant to rollover from year to year. Each year, the database needs to be truncated to support the upcoming summer reading club season.
* Alert all staff members that they should backup any important data, then perform the following database TRUNCATE operations in this order:
    * Will update this with a script to clear the data after this year's data is cleared.