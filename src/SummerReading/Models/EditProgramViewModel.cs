﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using SummerReadingData.Model;

namespace SummerReadingRewrite.Models
{
	public class EditProgramViewModel
	{
		public Program Program { get; set; }
		public Prize NewPrize { get; set; }
	}
}