﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using SummerReadingData.Model;

namespace SummerReadingRewrite.Models
{
	public class StaffHomeViewModel
	{
		public List<Patron> Patrons { get; set; }
		public List<Program> Programs { get; set; }
	}
}