﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using SummerReadingData.Model;

namespace SummerReadingRewrite.Models
{
    public class EditPatronViewModel
    {
        public Patron Patron { get; set; }
        public List<Prize> OwedPrizes { get; set; }

    }
}