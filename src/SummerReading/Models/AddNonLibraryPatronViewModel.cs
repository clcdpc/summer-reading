﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using SummerReadingData.Model;

namespace SummerReadingRewrite.Models
{
	public class AddNonLibraryPatronViewModel : Patron
	{
		public List<Program> Programs { get; set; }
        public int ProgramId { get; set; }
	}
}