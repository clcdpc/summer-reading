﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using SummerReadingData.Model;

namespace SummerReadingRewrite.Models
{
	public class AddLibraryPatronViewModel
	{
		public string Barcode { get; set; }
		public List<Program> Programs { get; set; }
		public int ProgramId { get; set; }
	}
}