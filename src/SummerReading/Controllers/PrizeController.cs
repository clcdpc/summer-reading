﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using SummerReadingData;
using SummerReadingData.Model;

namespace SummerReadingRewrite.Controllers
{
	[Authorize]
    public class PrizeController : Controller
    {
		private SummerReadingDb db = new SummerReadingDb();

        public ActionResult Award(int prizeid, int patronid)
        {
			var result = db.AwardPrize(patronid, prizeid);
			return PartialView("_AwardPrize", result);
        }

		//
		// GET: /Default2/Edit/5

		public ActionResult Edit(int id = 0)
		{
			Prize prize = db.Prizes.Find(id);
			if (prize == null)
			{
				return HttpNotFound();
			}
			ViewBag.ProgramId = new SelectList(db.Programs, "Id", "Name", prize.ProgramId);
			return View(prize);
		}

		//
		// POST: /Default2/Edit/5

		[HttpPost]
		public ActionResult Edit(Prize prize)
		{
			if (ModelState.IsValid)
			{
				db.Entry(prize).State = EntityState.Modified;
				db.SaveChanges();
				return RedirectToAction("Edit", "Program", new { Id = prize.ProgramId });
			}
			ViewBag.ProgramId = new SelectList(db.Programs, "Id", "Name", prize.ProgramId);
			return View(prize);
		}

		//
		// GET: /Default2/Delete/5

		public ActionResult Delete(int id = 0)
		{
			Prize prize = db.Prizes.Find(id);
			if (prize == null)
			{
				return HttpNotFound();
			}
			return View(prize);
		}

		//
		// POST: /Default2/Delete/5

		[HttpPost, ActionName("Delete")]
		public ActionResult DeleteConfirmed(int id)
		{
			Prize prize = db.Prizes.Find(id);
			db.Prizes.Remove(prize);
			db.SaveChanges();
			return RedirectToAction("Index");
		}

		protected override void Dispose(bool disposing)
		{
			db.Dispose();
			base.Dispose(disposing);
		}
    }
}
