﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using SummerReadingData.Model;
using SummerReadingData;

namespace SummerReadingRewrite.Controllers
{
	[CustomAuthorize(Roles="Admin")]
    public class StaffMemberController : Controller
    {
        private SummerReadingDb db = new SummerReadingDb();

        //
        // GET: /Default2/

        public ActionResult Index()
        {
            return View(db.GetStaffMembersForCurrentOrganization().ToList());
        }

        //
        // GET: /Default2/Details/5

        public ActionResult Details(int id = 0)
        {
            StaffMember staffmember = db.StaffMembers.Find(id);
            if (staffmember == null)
            {
                return HttpNotFound();
            }
            return View(staffmember);
        }

        //
        // GET: /Default2/Create

        public ActionResult Create()
        {
            var currentUser = db.GetCurrentStaffUser();
            var orgs = currentUser.Organization.ParentOrganization != null ?
                db.Organizations.Where(o => o.ParentOrganizationId == currentUser.Organization.ParentOrganizationId) :
                db.Organizations.Where(o => o.ParentOrganizationId == currentUser.OrganizationId);
            
			ViewBag.RoleId = new SelectList(db.Roles, "Id", "Name");
            ViewBag.OrgList = new SelectList(orgs, "Id", "Name");
            return View();
        }

        //
        // POST: /Default2/Create

        [HttpPost]
        public ActionResult Create(StaffMember staffmember, int roleId = 2 /* default to user */)
        {
            if (ModelState.IsValid)
            {
				var currentUser = db.GetCurrentStaffUser();
				if (!staffmember.Username.Contains('\\'))
				{
					var domain = currentUser.Organization.Domain.Length > 0 ? currentUser.Organization.Domain :
						currentUser.Organization.ParentOrganizationId != null ? currentUser.Organization.ParentOrganization.Domain :
						"";
					if (string.IsNullOrWhiteSpace(domain)) 
					{ 
						return View("Error", new HandleErrorInfo(new Exception("Can't find domain for user"), "StaffMember", "Create")); 
					}
					staffmember.Username = string.Format(@"{0}\{1}", currentUser.Organization.Domain, staffmember.Username);
				}
				staffmember.OrganizationId = currentUser.OrganizationId;
				staffmember.Roles = db.Roles.Where(r => r.Id == roleId).ToList();
                db.StaffMembers.Add(staffmember);
				
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(staffmember);
        }

        //
        // GET: /Default2/Edit/5

        public ActionResult Edit(int id = 0)
        {
            StaffMember staffmember = db.StaffMembers.Find(id);
            if (staffmember == null)
            {
                return HttpNotFound();
            }
            var currentUser = db.GetCurrentStaffUser();
            var orgs = currentUser.Organization.ParentOrganizationId != null ?
                db.Organizations.Where(o => o.ParentOrganizationId == currentUser.Organization.ParentOrganizationId) :
                db.Organizations.Where(o => o.ParentOrganizationId == currentUser.OrganizationId);
            ViewBag.RoleId = new SelectList(db.Roles, "Id", "Name", staffmember.Roles.First().Id);
            ViewBag.OrgList = new SelectList(orgs, "Id", "Name", staffmember.OrganizationId);
            return View(staffmember);
        }

        //
        // POST: /Default2/Edit/5

        [HttpPost]
        public ActionResult Edit(StaffMember staffmember)
        {
            if (ModelState.IsValid)
            {
                db.Entry(staffmember).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(staffmember);
        }

        //
        // GET: /Default2/Delete/5

        public ActionResult Delete(int id = 0)
        {
            StaffMember staffmember = db.StaffMembers.Find(id);
            if (staffmember == null)
            {
                return HttpNotFound();
            }
            return View(staffmember);
        }

        //
        // POST: /Default2/Delete/5

        [HttpPost, ActionName("Delete")]
        public ActionResult DeleteConfirmed(int id)
        {
            StaffMember staffmember = db.StaffMembers.Find(id);
            db.StaffMembers.Remove(staffmember);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            db.Dispose();
            base.Dispose(disposing);
        }
    }
}