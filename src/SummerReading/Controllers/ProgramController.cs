﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using SummerReadingData.Model;
using SummerReadingData;
using SummerReadingRewrite.Models;

namespace SummerReadingRewrite.Controllers
{
	[Authorize]
    public class ProgramController : Controller
    {
        private SummerReadingDb db = new SummerReadingDb();

        //
        // GET: /Program/

        public ActionResult Index()
        {
			var currentUser = db.GetCurrentStaffUser();
            var programs = db.GetProgramsForCurrentLibrary();
            return View(programs.ToList());
        }

        //
        // GET: /Program/Details/5

        public ActionResult Details(int id = 0)
        {
            Program program = db.Programs.Find(id);
            if (program == null)
            {
                return HttpNotFound();
            }
            return View(program);
        }

		[CustomAuthorize(Roles = "Admin")]
		public ActionResult Copy()
		{
			ViewBag.sourceId = new SelectList(db.GetProgramsForCurrentLibrary(), "Id", "Name");
			ViewBag.newOrgId = new SelectList(db.GetCurrentOrganizations(), "Id", "Name");
			return View();
		}

		[HttpPost]
		[CustomAuthorize(Roles = "Admin")]
		public ActionResult Copy(int sourceId, int newOrgId, string name)
		{
			var sourceProgram = db.Programs.Single(p => p.Id == sourceId);

			var prizes = new List<Prize>();
			sourceProgram.Prizes.ForEach(p => prizes.Add(
				new Prize
				{
					Description = p.Description,
					InitialQuantity = p.InitialQuantity,
					Requirement = p.Requirement,
					ProgramId = p.ProgramId
				})
			);
			var newProgram = new Program
			{
				Name = name.Length > 0 ? name : sourceProgram.Name,
				StartDate = sourceProgram.StartDate,
				EndDate = sourceProgram.EndDate,
				OrganizationId = newOrgId,
				MetricId = sourceProgram.MetricId,
				RecurringPrize = sourceProgram.RecurringPrize,
				Prizes = new List<Prize>(prizes)
			};

			db.Programs.Add(newProgram);
			db.SaveChanges();

			var test = db.Programs.Single(p => p.Id == newProgram.Id);

			ViewBag.EditMode = true;
			ViewBag.MetricId = new SelectList(db.Metrics, "Id", "Description", newProgram.MetricId);
			//return View("Edit", db.Programs.Single(p=>p.Id == newProgram.Id));
            return RedirectToAction("Edit", new { id = newProgram.Id });
		}


		[CustomAuthorize(Roles="Admin")]
        public ActionResult Create()
        {
            ViewBag.MetricList = new SelectList(db.Metrics, "Id", "Description");
			ViewBag.OrganizationId = new SelectList(db.GetCurrentOrganizations(), "Id", "Name");
			return View();
        }

		[CustomAuthorize(Roles = "Admin")]
		public ActionResult AddPrizeAjax(Prize prize)
		{
			if (ModelState.IsValid)
			{
				prize.CurrentQuantity = prize.InitialQuantity;
				db.Prizes.Add(prize);
				db.SaveChanges();
			}

			ViewBag.EditMode = true;
			return PartialView("_PrizeList", db.Prizes.Include(p=>p.Program).Where(p => p.ProgramId == prize.ProgramId));
		}

		[CustomAuthorize(Roles = "Admin")]
		public ActionResult AddActivityAjax(Activity activity)
		{
			if (ModelState.IsValid)
			{
				db.Activities.Add(activity);
				db.SaveChanges();
			}

			return PartialView("_ActivityList", db.Activities.Include(a=>a.Program).Where(a => a.ProgramId == activity.ProgramId));
		}

        //
        // POST: /Program/Create

        [HttpPost]
		[CustomAuthorize(Roles = "Admin")]
        public ActionResult Create(Program program)
        {
			var currentUser = db.GetCurrentStaffUser();
			//if (currentUser.OrganizationId != program.OrganizationId)
			//{
			//	ModelState.AddModelError("", "Program organization does not match staff member organization");
			//	return View(program);
			//}

            if (ModelState.IsValid)
            {
				//program.OrganizationId = currentUser.OrganizationId;
                db.Programs.Add(program);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            ViewBag.OrganizationId = new SelectList(db.GetCurrentOrganizations(), "Id", "Name", program.OrganizationId);
            return View(program);
        }

        //
        // GET: /Program/Edit/5
		[CustomAuthorize(Roles = "Admin")]
        public ActionResult Edit(int id = 0)
        {
            Program program = db.Programs.Find(id);
            if (program == null)
            {
                return HttpNotFound();
            }

			ViewBag.EditMode = true;
			ViewBag.MetricId = new SelectList(db.Metrics, "Id", "Description", program.MetricId);
			return View(program);
        }

        //
        // POST: /Program/Edit/5

        [HttpPost]
		[CustomAuthorize(Roles = "Admin")]
        public ActionResult Edit(Program program)
        {			
            if (ModelState.IsValid)
            {
                db.Entry(program).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.OrganizationId = new SelectList(db.Organizations, "Id", "Name", program.OrganizationId);
            return View(program);
        }

        //
        // GET: /Program/Delete/5
		[CustomAuthorize(Roles = "Admin")]
        public ActionResult Delete(int id = 0)
        {
            Program program = db.Programs.Find(id);
            if (program == null)
            {
                return HttpNotFound();
            }
            return View(program);
        }

        //
        // POST: /Program/Delete/5

        [HttpPost, ActionName("Delete")]
		[CustomAuthorize(Roles = "Admin")]
        public ActionResult DeleteConfirmed(int id)
        {
            Program program = db.Programs.Find(id);

            db.ReadingLogs.Where(rl => rl.ProgramId == id).ToList().ForEach(rl => db.ReadingLogs.Remove(rl));
            
			db.AwardedPrizes.Include(ap=>ap.Patron)
				.Include(ap => ap.Prize)
				.Where(ap => ap.Prize.ProgramId == id)// || ap.Patron.CurrentProgram.Id == id)
				.ToList()
				.ForEach(ap => db.AwardedPrizes.Remove(ap));

            db.Prizes.Where(p => p.ProgramId == id).ToList().ForEach(p => db.Prizes.Remove(p));
            db.Patrons.Where(p => p.Registrations.OrderByDescending(r=>r.RegistrationDate).FirstOrDefault().ProgramId == id).ToList().ForEach(p => db.Patrons.Remove(p));
			//db.Prizes.Where(p => p.ProgramId == id).ToList().ForEach(p => db.Prizes.Remove(p));
					
            db.Programs.Remove(program);
			
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            db.Dispose();
            base.Dispose(disposing);
        }
    }
}