﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Clc.Polaris.Api;
using SummerReadingData.Model;
using SummerReadingRewrite.Models;
using SummerReadingData;
using System.Linq.Expressions;

namespace SummerReadingRewrite.Controllers
{
	[Authorize]
    public class PatronController : Controller
    {
		SummerReadingDb db = new SummerReadingDb();

		public ActionResult Index()
		{
			var currentUser = db.GetCurrentStaffUser();
			return View(db.GetPatronsForCurrentLibrary());
		}   

		[HttpPost]
		public ActionResult AddNonLibraryPatron(Patron patron, int programId)
		{			
			if (db.Patrons.Any(p => p.Username == patron.Username))
			{
				ModelState.AddModelError("", "Username is already in use, please try again");
				var viewModel = new AddNonLibraryPatronViewModel
				{
					Programs = db.GetProgramsForCurrentLibrary().ToList()
				};
				return View("AddNonLibraryPatron", viewModel);
			}

			if (ModelState.IsValid)
			{               
				db.Patrons.Add(patron);                
				db.SaveChanges();

                patron.Register(programId);
                db.SaveChanges();
			}
            db = new SummerReadingDb();

            patron = db.Patrons.Single(p => p.Id == patron.Id);

            return View("PatronAddedSuccessfully", patron);
		}

		[HttpGet]
		public ActionResult AddNonLibraryPatron()
		{
			var viewModel = new AddNonLibraryPatronViewModel
			{
				Programs = db.GetProgramsForCurrentLibrary().ToList()
			};

			return View(viewModel);
		}

		public ActionResult Search(string term) 
		{
			if (string.IsNullOrWhiteSpace(term))
			{
				return View("Index", db.GetPatronsForCurrentLibrary().ToList());
			}

			if (db.Patrons.Any(p => p.Username == term))
			{
				return RedirectToAction("Details", new { id = db.Patrons.Single(p => p.Username == term).Id });
			}

			if (term.Contains(" "))
			{
				var fname = term.Split(' ')[0];
				var lname = term.Split(' ')[1];
				if (db.Patrons.Count(p => p.FirstName == fname && p.LastName == lname) == 1)
				{
					
					return RedirectToAction("Details", new { id = db.Patrons.SingleOrDefault(p => p.FirstName == fname && p.LastName == lname).Id });					
				}
			}
            
			var staffMember = db.GetCurrentStaffUser();

			var patrons = db.Patrons.Where(p => (new List<string> { p.FirstName, p.LastName }).Any(f => term.Contains(f)) || 
				(new List<string> { p.FirstName, p.LastName }).Any(f => f.Contains(term)))
				.Where(p=>p.Registrations.FirstOrDefault().Program.Organization.ParentOrganizationId == (staffMember.Organization.ParentOrganizationId ?? staffMember.Organization.Id));

            return View("Index", patrons.ToList());
		}

		public ActionResult GetPrizeList(int patronId)
		{
			return PartialView("_PatronPrizeList", 
				db.Patrons
				.Include(p=>p.Registrations)
                .Include(p=>p.ReadingLogs)
				.Include(p=>p.ReceivedPrizes)
				.Single(p => p.Id == patronId)
			);
		}

		public ActionResult AjaxAddReadingLog(ReadingLog log)
		{
			if (ModelState.IsValid)
			{
				if (log.ActivityId != null)
				{
					log.Activity = db.Activities.Single(a => a.Id == log.ActivityId);
					log.Value = log.Activity.Value;
				}
				
				db.ReadingLogs.Add(log);
				db.SaveChanges();
			}

			return PartialView("_ReadingLogList", db.ReadingLogs.Where(rl=>rl.PatronId == log.PatronId).Include(rl=>rl.Program).OrderByDescending(rl=>rl.Id));
		}

		public ActionResult AddLibraryPatron()
		{
			var viewModel = new AddLibraryPatronViewModel
			{
				Programs = db.GetProgramsForCurrentLibrary().ToList()
			};

			return View(viewModel);
		}

		[HttpPost]
		public ActionResult AddLibraryPatron(string barcode, int programid)
		{
			if (db.Patrons.Any(p => p.Username == barcode))
			{
				ModelState.AddModelError("", "Username is already in use, please try again");
				var viewModel = new AddLibraryPatronViewModel
				{
                    Barcode = barcode,
					Programs = db.GetProgramsForCurrentLibrary().ToList()
				};
				return View("AddLibraryPatron", viewModel);
			}

			if (ModelState.IsValid)
			{
				var polaris = new PolarisApiClient();
				var result = polaris.staff_PatronBasicDataGet(barcode);

				if (result.PatronBasicData == null)
				{
					return View("GeneralError", result.ErrorMessage);
				}

				var patronData = result.PatronBasicData;

				var patron = new Patron
				{
					FirstName = patronData.NameFirst,
					LastName = patronData.NameLast,
					Username = barcode,
					Email = patronData.EmailAddress,
					Phone = patronData.PhoneNumber,
					Birthdate = patronData.BirthDate
				};

                
                db.Patrons.Add(patron);
                db.SaveChanges();

                patron.Register(programid);
                db.SaveChanges();

                db = new SummerReadingDb();

                patron = db.Patrons.Single(p => p.Id == patron.Id);

				return View("PatronAddedSuccessfully", patron);
			}

			return View();
		}

		public ActionResult Details(int id = 0)
		{
			Patron patron = db.Patrons.Find(id);
            
			if (patron == null)
			{
				return HttpNotFound();
			}
			if (patron.Registrations.First().Program.Metric.Description.StartsWith("Poi"))
			{
                var programid = patron.Registrations.First().Program.Id;

                ViewBag.ActivityId = new SelectList(db.Activities.Where(a => a.ProgramId == programid).ToList(), "Id", "Description");
			}
			return View(patron);
		}

		public ActionResult Edit(int id = 0)
		{
			Patron patron = db.Patrons.Include("Registrations").Single(p=>p.Id == id);
			if (patron == null)
			{
				return HttpNotFound();
			}
			ViewBag.ProgramId = new SelectList(db.GetProgramsForCurrentLibrary(), "Id", "Name", patron.Registrations == null || patron.Registrations.Count == 0 ? 0 : patron.CurrentProgram.Id);
			return View(patron);
		}

		[HttpPost]
        public ActionResult Edit(Patron patron, int programid)
		{
			if (ModelState.IsValid)
			{
                db.Patrons.Attach(patron);
                db.Entry(patron).Collection(p => p.Registrations).Load();
                db.Entry(patron).State = EntityState.Modified;

                if (db.ProgramRegistrations.Any(pr => pr.PatronId == patron.Id && pr.ProgramId == patron.CurrentProgram.Id))
                {
                    db.ProgramRegistrations.Remove(db.ProgramRegistrations.Single(pr => pr.PatronId == patron.Id && pr.ProgramId == patron.CurrentProgram.Id));
                }
                patron.Register(programid);
               

				db.SaveChanges();
				return RedirectToAction("Index");
			}
			ViewBag.ProgramId = new SelectList(db.Programs, "Id", "Name", patron.Registrations.First().Program.Id);
			return View(patron);
		}

		public ActionResult Delete(int id = 0)
		{
			Patron patron = db.Patrons.Find(id);
			if (patron == null)
			{
				return HttpNotFound();
			}
			return View(patron);
		}
		
		[HttpPost, ActionName("Delete")]
		public ActionResult DeleteConfirmed(int id)
		{
			Patron patron = db.Patrons.Find(id);
			db.AwardedPrizes.Where(ap => ap.PatronId == id).ToList().ForEach(ap => db.AwardedPrizes.Remove(ap));
			db.Patrons.Remove(patron);
			db.SaveChanges();
			return RedirectToAction("Index");
		}

		protected override void Dispose(bool disposing)
		{
			db.Dispose();
			base.Dispose(disposing);
		}
    }
}
