﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Clc.Polaris.Api;
using SummerReadingData;
using SummerReadingRewrite.Models;

namespace SummerReadingRewrite.Controllers
{
    [CustomAuthorize]
	public class HomeController : Controller
	{
		private SummerReadingDb db = new SummerReadingDb();

		[CustomAuthorize]
		public ActionResult Index()
		{
			var currentUser = db.GetCurrentStaffUser();

			var viewModel = new StaffHomeViewModel
			{
				Patrons = db.GetPatronsForCurrentLibrary().ToList(),
				Programs = db.GetProgramsForCurrentLibrary().OrderBy(p=>p.OrganizationId).ToList()
			};
            		
			return View(viewModel);
		}

        [AllowAnonymous]
        public ActionResult Warmup()
        {
            return Content("");
        }
	}
}
