CREATE DATABASE [SummerReading]
GO

ALTER DATABASE [SummerReading] SET COMPATIBILITY_LEVEL = 120
GO

USE [SummerReading]
GO
/****** Object:  Table [dbo].[Activities]    Script Date: 5/18/2016 7:02:34 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Activities](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Value] [int] NOT NULL,
	[Description] [nvarchar](max) NOT NULL,
	[ProgramId] [int] NOT NULL,
 CONSTRAINT [PK_dbo.Activities] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
/****** Object:  Table [dbo].[AwardedPrizes]    Script Date: 5/18/2016 7:02:35 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[AwardedPrizes](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[PatronId] [int] NOT NULL,
	[PrizeId] [int] NOT NULL,
	[DateAwarded] [datetime] NULL,
 CONSTRAINT [PK_dbo.AwardedPrizes] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[Metrics]    Script Date: 5/18/2016 7:02:35 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Metrics](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Description] [nvarchar](max) NULL,
 CONSTRAINT [PK_dbo.Metrics] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
/****** Object:  Table [dbo].[Organizations]    Script Date: 5/18/2016 7:02:35 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Organizations](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Name] [nvarchar](max) NULL,
	[Domain] [nvarchar](max) NULL,
	[ParentOrganizationId] [int] NULL,
 CONSTRAINT [PK_dbo.Organizations] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
/****** Object:  Table [dbo].[Patrons]    Script Date: 5/18/2016 7:02:35 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Patrons](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[FirstName] [nvarchar](max) NOT NULL,
	[LastName] [nvarchar](max) NOT NULL,
	[Username] [nvarchar](max) NOT NULL,
	[Birthdate] [datetime] NULL,
	[Email] [nvarchar](max) NULL,
	[Phone] [nvarchar](max) NULL,
 CONSTRAINT [PK_dbo.Patrons] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
/****** Object:  Table [dbo].[Prizes]    Script Date: 5/18/2016 7:02:35 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Prizes](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[ProgramId] [int] NOT NULL,
	[Description] [nvarchar](max) NOT NULL,
	[InitialQuantity] [int] NOT NULL,
	[CurrentQuantity] [int] NOT NULL,
	[Requirement] [int] NOT NULL,
	[QuantityRemaining] [int] NULL,
	[Discriminator] [nvarchar](128) NOT NULL,
 CONSTRAINT [PK_dbo.Prizes] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
/****** Object:  Table [dbo].[ProgramRegistrations]    Script Date: 5/18/2016 7:02:35 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[ProgramRegistrations](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[PatronId] [int] NOT NULL,
	[ProgramId] [int] NOT NULL,
	[RegistrationDate] [datetime] NOT NULL,
 CONSTRAINT [PK_dbo.ProgramRegistrations] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[Programs]    Script Date: 5/18/2016 7:02:35 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Programs](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Name] [nvarchar](max) NOT NULL,
	[StartDate] [datetime] NOT NULL,
	[EndDate] [datetime] NOT NULL,
	[OrganizationId] [int] NOT NULL,
	[MetricId] [int] NOT NULL,
	[RecurringPrize] [bit] NOT NULL,
 CONSTRAINT [PK_dbo.Programs] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
/****** Object:  Table [dbo].[ReadingLogs]    Script Date: 5/18/2016 7:02:35 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[ReadingLogs](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[LogTime] [datetime] NOT NULL,
	[PatronId] [int] NOT NULL,
	[ProgramId] [int] NOT NULL,
	[Value] [int] NOT NULL,
	[BookTitle] [nvarchar](max) NULL,
	[ActivityId] [int] NULL,
 CONSTRAINT [PK_dbo.ReadingLogs] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
/****** Object:  Table [dbo].[Roles]    Script Date: 5/18/2016 7:02:35 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Roles](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Name] [nvarchar](max) NULL,
	[Description] [nvarchar](max) NULL,
 CONSTRAINT [PK_dbo.Roles] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
/****** Object:  Table [dbo].[RoleStaffMembers]    Script Date: 5/18/2016 7:02:35 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[RoleStaffMembers](
	[Role_Id] [int] NOT NULL,
	[StaffMember_Id] [int] NOT NULL,
 CONSTRAINT [PK_dbo.RoleStaffMembers] PRIMARY KEY CLUSTERED 
(
	[Role_Id] ASC,
	[StaffMember_Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[StaffMembers]    Script Date: 5/18/2016 7:02:35 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[StaffMembers](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Username] [nvarchar](max) NOT NULL,
	[OrganizationId] [int] NOT NULL,
 CONSTRAINT [PK_dbo.StaffMembers] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
ALTER TABLE [dbo].[Activities]  WITH CHECK ADD  CONSTRAINT [FK_dbo.Activities_dbo.Programs_ProgramId] FOREIGN KEY([ProgramId])
REFERENCES [dbo].[Programs] ([Id])
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[Activities] CHECK CONSTRAINT [FK_dbo.Activities_dbo.Programs_ProgramId]
GO
ALTER TABLE [dbo].[AwardedPrizes]  WITH CHECK ADD  CONSTRAINT [FK_dbo.AwardedPrizes_dbo.Patrons_PatronId] FOREIGN KEY([PatronId])
REFERENCES [dbo].[Patrons] ([Id])
GO
ALTER TABLE [dbo].[AwardedPrizes] CHECK CONSTRAINT [FK_dbo.AwardedPrizes_dbo.Patrons_PatronId]
GO
ALTER TABLE [dbo].[AwardedPrizes]  WITH CHECK ADD  CONSTRAINT [FK_dbo.AwardedPrizes_dbo.Prizes_PrizeId] FOREIGN KEY([PrizeId])
REFERENCES [dbo].[Prizes] ([Id])
GO
ALTER TABLE [dbo].[AwardedPrizes] CHECK CONSTRAINT [FK_dbo.AwardedPrizes_dbo.Prizes_PrizeId]
GO
ALTER TABLE [dbo].[Organizations]  WITH CHECK ADD  CONSTRAINT [FK_dbo.Organizations_dbo.Organizations_ParentOrganizationId] FOREIGN KEY([ParentOrganizationId])
REFERENCES [dbo].[Organizations] ([Id])
GO
ALTER TABLE [dbo].[Organizations] CHECK CONSTRAINT [FK_dbo.Organizations_dbo.Organizations_ParentOrganizationId]
GO
ALTER TABLE [dbo].[Prizes]  WITH CHECK ADD  CONSTRAINT [FK_dbo.Prizes_dbo.Programs_ProgramId] FOREIGN KEY([ProgramId])
REFERENCES [dbo].[Programs] ([Id])
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[Prizes] CHECK CONSTRAINT [FK_dbo.Prizes_dbo.Programs_ProgramId]
GO
ALTER TABLE [dbo].[ProgramRegistrations]  WITH CHECK ADD  CONSTRAINT [FK_dbo.ProgramRegistrations_dbo.Patrons_PatronId] FOREIGN KEY([PatronId])
REFERENCES [dbo].[Patrons] ([Id])
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[ProgramRegistrations] CHECK CONSTRAINT [FK_dbo.ProgramRegistrations_dbo.Patrons_PatronId]
GO
ALTER TABLE [dbo].[ProgramRegistrations]  WITH CHECK ADD  CONSTRAINT [FK_dbo.ProgramRegistrations_dbo.Programs_ProgramId] FOREIGN KEY([ProgramId])
REFERENCES [dbo].[Programs] ([Id])
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[ProgramRegistrations] CHECK CONSTRAINT [FK_dbo.ProgramRegistrations_dbo.Programs_ProgramId]
GO
ALTER TABLE [dbo].[Programs]  WITH CHECK ADD  CONSTRAINT [FK_dbo.Programs_dbo.Metrics_MetricId] FOREIGN KEY([MetricId])
REFERENCES [dbo].[Metrics] ([Id])
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[Programs] CHECK CONSTRAINT [FK_dbo.Programs_dbo.Metrics_MetricId]
GO
ALTER TABLE [dbo].[Programs]  WITH CHECK ADD  CONSTRAINT [FK_dbo.Programs_dbo.Organizations_OrganizationId] FOREIGN KEY([OrganizationId])
REFERENCES [dbo].[Organizations] ([Id])
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[Programs] CHECK CONSTRAINT [FK_dbo.Programs_dbo.Organizations_OrganizationId]
GO
ALTER TABLE [dbo].[ReadingLogs]  WITH CHECK ADD  CONSTRAINT [FK_dbo.ReadingLogs_dbo.Activities_ActivityId] FOREIGN KEY([ActivityId])
REFERENCES [dbo].[Activities] ([Id])
GO
ALTER TABLE [dbo].[ReadingLogs] CHECK CONSTRAINT [FK_dbo.ReadingLogs_dbo.Activities_ActivityId]
GO
ALTER TABLE [dbo].[ReadingLogs]  WITH CHECK ADD  CONSTRAINT [FK_dbo.ReadingLogs_dbo.Patrons_PatronId] FOREIGN KEY([PatronId])
REFERENCES [dbo].[Patrons] ([Id])
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[ReadingLogs] CHECK CONSTRAINT [FK_dbo.ReadingLogs_dbo.Patrons_PatronId]
GO
ALTER TABLE [dbo].[ReadingLogs]  WITH CHECK ADD  CONSTRAINT [FK_dbo.ReadingLogs_dbo.Programs_ProgramId] FOREIGN KEY([ProgramId])
REFERENCES [dbo].[Programs] ([Id])
GO
ALTER TABLE [dbo].[ReadingLogs] CHECK CONSTRAINT [FK_dbo.ReadingLogs_dbo.Programs_ProgramId]
GO
ALTER TABLE [dbo].[RoleStaffMembers]  WITH CHECK ADD  CONSTRAINT [FK_dbo.RoleStaffMembers_dbo.Roles_Role_Id] FOREIGN KEY([Role_Id])
REFERENCES [dbo].[Roles] ([Id])
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[RoleStaffMembers] CHECK CONSTRAINT [FK_dbo.RoleStaffMembers_dbo.Roles_Role_Id]
GO
ALTER TABLE [dbo].[RoleStaffMembers]  WITH CHECK ADD  CONSTRAINT [FK_dbo.RoleStaffMembers_dbo.StaffMembers_StaffMember_Id] FOREIGN KEY([StaffMember_Id])
REFERENCES [dbo].[StaffMembers] ([Id])
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[RoleStaffMembers] CHECK CONSTRAINT [FK_dbo.RoleStaffMembers_dbo.StaffMembers_StaffMember_Id]
GO
ALTER TABLE [dbo].[StaffMembers]  WITH CHECK ADD  CONSTRAINT [FK_dbo.StaffMembers_dbo.Organizations_OrganizationId] FOREIGN KEY([OrganizationId])
REFERENCES [dbo].[Organizations] ([Id])
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[StaffMembers] CHECK CONSTRAINT [FK_dbo.StaffMembers_dbo.Organizations_OrganizationId]
GO

SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[AddStaffMember]
	-- Add the parameters for the stored procedure here
	@username varchar(255), 
	@orgId_FROM_SUMMER_READING_DB_TABLE int,
	@isAdmin tinyint
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	declare @output table (id int)
	declare @userid int

	insert into SummerReading.dbo.StaffMembers
	output INSERTED.Id into @output
	values (@username, @orgId_FROM_SUMMER_READING_DB_TABLE)

	select @userid = id from @output

	insert into SummerReading.dbo.RoleStaffMembers
	values (2 - @isAdmin, @userid)
END

GO