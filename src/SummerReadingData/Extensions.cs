﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.Validation;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using SummerReadingData.Model;
using System.Data.Objects;

namespace SummerReadingData
{
	public static class Extensions
	{
		public static StaffMember _GetCurrent(this IQueryable<StaffMember> query) 
		{
			return query.SingleOrDefault(s => s.Username == HttpContext.Current.User.Identity.Name);
		}

        public static Patron Register(this Patron p, int programId)
        {
            using (var db = new SummerReadingDb())
            {
                p.Registrations = p.Registrations ?? new List<ProgramRegistration>();
                p.Registrations.Add(new ProgramRegistration { ProgramId = programId, RegistrationDate = DateTime.Now.Date });
                return p;
            }
        }

		public static StaffMember GetCurrentStaffUser(this SummerReadingDb db)
		{
			var user = db.StaffMembers.SingleOrDefault(s => s.Username == HttpContext.Current.User.Identity.Name);
			var domain = HttpContext.Current.User.Identity.Name.Split('\\')[0];

            if (user == null)
            {
                user = new StaffMember
                {
                    Username = HttpContext.Current.User.Identity.Name,
                    Organization = db.Organizations.Single(o => o.Domain == domain && o.ParentOrganizationId == null)
                };
            }
            return user;
		}

		public static IQueryable<Program> GetProgramsForCurrentLibrary(this SummerReadingDb db)
		{
			var currentOrg = db.GetCurrentStaffUser().Organization;

            if (currentOrg.ParentOrganizationId != null) // we're currently at a branch, not a library
            {
                return db.Programs.Where(p => p.OrganizationId == currentOrg.Id);
            }

            // we're currently at a library, so show everything
            return db.Programs.Where(p => p.Organization.ParentOrganizationId == currentOrg.Id);
		}

		public static IQueryable<StaffMember> GetStaffMembersForCurrentOrganization(this SummerReadingDb db) 
		{
			var currentOrg = db.GetCurrentStaffUser().Organization;
			int orgid = currentOrg.ParentOrganizationId ?? currentOrg.Id;
			return db.StaffMembers.Where(s => s.OrganizationId == orgid || s.Organization.ParentOrganizationId == orgid);
		}

		public static IQueryable<Patron> GetPatronsForCurrentLibrary(this SummerReadingDb db)
		{
			var currentOrg = db.GetCurrentStaffUser().Organization;
			int orgid = currentOrg.ParentOrganizationId ?? currentOrg.Id;
			return db.Patrons.Where(p => p.Registrations.FirstOrDefault().Program.OrganizationId == orgid || p.Registrations.FirstOrDefault().Program.Organization.ParentOrganizationId == orgid);
		}

		public static IQueryable<Organization> GetCurrentOrganizations(this SummerReadingDb db)
		{
			var currentOrg = db.GetCurrentStaffUser().Organization;
			int orgid = currentOrg.ParentOrganizationId ?? currentOrg.Id;
			return db.Organizations.Where(o => o.ParentOrganizationId == orgid);
		}

		public static IQueryable<Prize> GetPatronsEarnedPrizes(this SummerReadingDb db, Patron patron)
		{
            var programid = patron.Registrations.FirstOrDefault().Program.Id;
			return db.Prizes.Where(p => p.ProgramId == programid && p.Requirement <= db.ReadingLogs.Where(rl => rl.PatronId == patron.Id).Sum(rl => rl.Value));
		}

		public static IQueryable<AwardedPrize> GetPatronsReceivedPrizes(this SummerReadingDb db, Patron patron)
		{
			return db.AwardedPrizes.Where(ap => ap.PatronId == patron.Id);
		}

		public static List<Prize> GetAllPrizesForPatron(this SummerReadingDb db, Patron patron) 
		{
			var prizes = new List<Prize>();

			prizes.AddRange(db.GetPatronsEarnedPrizes(patron));
			prizes.AddRange(patron.Registrations.FirstOrDefault().Program.Prizes.Where(p => !prizes.Select(p2 => p2.Id).Contains(p.Id)));

			return prizes.OrderBy(p=>p.Requirement).ToList();
		}

		public static int GetNumberOfEarnedRecurringPrize(this SummerReadingDb db, Patron patron, Prize prize)
		{
			var logSum = patron.ReadingLogs.Sum(rl => rl.Value);
			return (int)(logSum / prize.Requirement);
		}

		public static int GetNumberOfReceivedRecurringPrize(this SummerReadingDb db, Patron patron, Prize prize)
		{
			return prize.PrizesAwarded.Where(p => p.PatronId == patron.Id && p.PrizeId == prize.Id).Count();
		}

		public static bool PatronOwedPrize(this SummerReadingDb db, Patron patron, Prize prize)
		{
			if (patron.Registrations.FirstOrDefault().Program.RecurringPrize)
			{
				return (db.GetNumberOfEarnedRecurringPrize(patron, prize) - db.GetNumberOfReceivedRecurringPrize(patron, prize)) > 0;
			}

			return patron.ReadingLogs.Sum(rl => rl.Value) >= prize.Requirement && (!db.AwardedPrizes.Any(ap=>ap.PatronId == patron.Id && ap.PrizeId == prize.Id));
		}

		public static AwardPrizeResult AwardPrize(this SummerReadingDb db, int patronId, int prizeId)
		{
			return db.AwardPrize(db.Patrons.Single(p => p.Id == patronId), db.Prizes.Single(p => p.Id == prizeId));
		}

		public static AwardPrizeResult AwardPrize(this SummerReadingDb db, Patron patron, Prize prize)
		{
			if (patron.Registrations.FirstOrDefault().Program.Id != prize.ProgramId)
			{
				return new AwardPrizeFailure(string.Format("Prize {0} is not a valid prize for program {1}.", prize.Description, patron.Registrations.FirstOrDefault().Program.Name), prize);
			}

			if (patron.ReadingLogs.Sum(rl => rl.Value) < prize.Requirement)
			{
				return new AwardPrizeFailure(
					string.Format(
						"Patron does not meet the requirement for prize {0}. Required: {1} | Current: {2}",
						prize.Description,
						prize.Requirement,
						patron.ReadingLogs.Sum(rl => rl.Value)
					),
					prize
				);
			}			

			if (patron.Registrations.FirstOrDefault().Program.RecurringPrize) 
			{
				var numToAward = db.GetNumberOfEarnedRecurringPrize(patron, prize) - db.GetNumberOfReceivedRecurringPrize(patron, prize);

				if (numToAward > 0)
				{
					for (int i = 1; i <= numToAward; i++)
					{
						db.AwardedPrizes.Add(new AwardedPrize { PatronId = patron.Id, PrizeId = prize.Id, DateAwarded = DateTime.Now });
						if (prize.InitialQuantity > 0) prize.CurrentQuantity--;
					}
					db.SaveChanges();
					return new AwardPrizeSuccess(string.Format("Patron was awarded {0} prizes", numToAward), prize);
				}

				return new AwardPrizeFailure("Patron has been awarded all earned prizes.", prize);
			}

			if (db.AwardedPrizes.Any(ap => ap.PrizeId == prize.Id && ap.PatronId == patron.Id))
			{
				return new AwardPrizeFailure("Patron has already received this prize.", prize);
			}

			db.AwardedPrizes.Add(new AwardedPrize { PatronId = patron.Id, PrizeId = prize.Id, DateAwarded = DateTime.Now });
			if (prize.InitialQuantity > 0) prize.CurrentQuantity--;
			db.SaveChanges();

			return new AwardPrizeSuccess("Prize successfully awarded.", prize);
		}

		public static int SaveChangesWithErrors(this DbContext context)
		{
			try
			{
				return context.SaveChanges();
			}
			catch (DbEntityValidationException ex)
			{
				StringBuilder sb = new StringBuilder();

				foreach (var failure in ex.EntityValidationErrors)
				{
					sb.AppendFormat("{0} failed validation\n", failure.Entry.Entity.GetType());
					foreach (var error in failure.ValidationErrors)
					{
						sb.AppendFormat("- {0} : {1}", error.PropertyName, error.ErrorMessage);
						sb.AppendLine();
					}
				}

				throw new DbEntityValidationException(
					"Entity Validation Failed - errors follow:\n" +
					sb.ToString(), ex
				); // Add the original exception as the innerException
			}
		}
	}

}
