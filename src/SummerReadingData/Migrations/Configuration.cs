namespace SummerReadingData.Migrations
{
	using System;
	using System.Collections.Generic;
	using System.Data.Entity;
	using System.Data.Entity.Migrations;
	using System.Data.Entity.Validation;
	using System.Diagnostics;
	using System.Linq;
	using System.Text;
	using SummerReadingData.Model;

    public class Configuration : DbMigrationsConfiguration<SummerReadingData.SummerReadingDb>
    {
        public Configuration()
        {
            AutomaticMigrationsEnabled = true;
        }

        protected override void Seed(SummerReadingData.SummerReadingDb context)
        {
            //  This method will be called after migrating to the latest version.
        }		
    }
}
