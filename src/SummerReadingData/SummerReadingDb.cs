﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SummerReadingData.Model;

namespace SummerReadingData
{
	public class SummerReadingDb : DbContext
	{
		public DbSet<Patron> Patrons { get; set; }
		public DbSet<Program> Programs { get; set; }
		public DbSet<Activity> Activities { get; set; }
		public DbSet<Metric> Metrics { get; set; }
		public DbSet<Prize> Prizes { get; set; }
		public DbSet<AwardedPrize> AwardedPrizes { get; set; }
		public DbSet<Organization> Organizations { get; set; }
		public DbSet<ReadingLog> ReadingLogs { get; set; }
		public DbSet<StaffMember> StaffMembers { get; set; }
		public DbSet<Role> Roles { get; set; }
        public DbSet<ProgramRegistration> ProgramRegistrations { get; set; }

		public SummerReadingDb() : base("SummerReadingDb")
		{

		}

		protected override void OnModelCreating(DbModelBuilder modelBuilder)
		{
			modelBuilder.Entity<ReadingLog>().HasRequired(rl => rl.Program).WithMany(p => p.ReadingLogs).WillCascadeOnDelete(false);
			modelBuilder.Entity<AwardedPrize>().HasRequired(ap => ap.Prize).WithMany(p => p.PrizesAwarded).WillCascadeOnDelete(false);
			modelBuilder.Entity<AwardedPrize>().HasRequired(ap => ap.Patron).WithMany(p => p.ReceivedPrizes).WillCascadeOnDelete(false);
			modelBuilder.Entity<Organization>().HasOptional(o => o.ParentOrganization).WithMany().HasForeignKey(o => o.ParentOrganizationId);
			base.OnModelCreating(modelBuilder);
		}
	}
}
