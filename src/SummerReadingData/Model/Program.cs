﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;

namespace SummerReadingData.Model
{
	public class Program
	{
		public int Id { get; set; }
		[Required]
		public string Name { get; set; }
		[Required]
		[DataType("date")]
		[DisplayFormat(DataFormatString = "{0:d}")]
		[Display(Name="Starts")]
		public DateTime StartDate { get; set; }
		[Required]
		[DataType("date")]
		[DisplayFormat(DataFormatString = "{0:d}")]
		[Display(Name="Ends")]
		public DateTime EndDate { get; set; }
		[Required]
		public int OrganizationId { get; set; }
		[ForeignKey("OrganizationId")]
		public virtual Organization Organization { get; set; }
		[Required]
		public int MetricId { get; set; }
		[ForeignKey("MetricId")]
		public virtual Metric Metric { get; set; }
		public virtual List<ReadingLog> ReadingLogs { get; set; }
		public virtual List<Prize> Prizes { get; set; }
		public virtual List<Activity> Activities { get; set; }
        public virtual List<ProgramRegistration> Registrations { get; set; }
		public bool RecurringPrize { get; set; }
	}
}
