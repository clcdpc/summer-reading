﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SummerReadingData.Model
{
	public class Prize
	{
		public int Id { get; set; }
		[Required]
		public int ProgramId { get; set; }
		public Program Program { get; set; }
		[Required]
		public string Description { get; set; }
		[DataType("int")]
		[Range(0, 10000)]
		[Required]
		public int InitialQuantity { get; set; }
		public int CurrentQuantity { get; set; }
		[DataType("int")]
		[Range(1, 10000)]
		[Required]
		public int Requirement { get; set; }
		public virtual List<AwardedPrize> PrizesAwarded { get; set; }
	}
}
