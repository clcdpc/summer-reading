﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SummerReadingData.Model
{
	public class AwardPrizeResult
	{
		public bool Success { get; set; }
		public string Message { get; set; }
		public Prize Prize { get; set; }
	}

	public class AwardPrizeFailure : AwardPrizeResult 
	{
		public AwardPrizeFailure()
		{

		}

		public AwardPrizeFailure(string Message, Prize Prize)
		{
			Success = false;
			this.Message = Message;
			this.Prize = Prize;
		}
	}
	public class AwardPrizeSuccess : AwardPrizeResult 
	{ 
		public AwardPrizeSuccess()
		{

		}

		public AwardPrizeSuccess(string Message, Prize prize)
		{
			Success = true;
			this.Message = Message;
			this.Prize = prize;
		}
	}
}
