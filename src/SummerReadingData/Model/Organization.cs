﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SummerReadingData.Model
{
	public class Organization
	{
		public int Id { get; set; }
		public string Name { get; set; }
		public string Domain { get; set; }
		public virtual List<StaffMember> StaffMembers { get; set; }

		public int? ParentOrganizationId { get; set; }
		public Organization ParentOrganization { get; set; }
	}
}
