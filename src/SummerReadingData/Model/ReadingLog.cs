﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SummerReadingData.Model
{
	public class ReadingLog
	{
		public int Id { get; set; }
		[DisplayName("Date")]
		[Required]
		public DateTime LogTime { get; set; }
		public int PatronId { get; set; }
		public virtual Patron Patron { get; set; }
		public int ProgramId { get; set; }
		public virtual Program Program { get; set; }
		[DisplayName("Value")]
		[Range(1, 1440)]
		[Required]
		public int Value { get; set; }
		[DisplayName("Book Title (optional)")]
		public string BookTitle { get; set; }
		public int? ActivityId { get; set; }
		public virtual Activity Activity { get; set; }

		public ReadingLog()
		{
			LogTime = DateTime.Now;
		}
	}
}
