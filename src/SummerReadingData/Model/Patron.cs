﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SummerReadingData.Model
{
    public class Patron
    {
		[DisplayName("Patron ID")]
		public int Id { get; set; }
		[Required]
		[DisplayName("First Name")]
		public string FirstName { get; set; }
		[Required]
		[DisplayName("Last Name")]
		public string LastName { get; set; }
		[Required]
		[DisplayName("Username")]
		public string Username { get; set; }
		[DataType("date")]
		[DisplayFormat(DataFormatString = "{0:d}")]
		public DateTime? Birthdate { get; set; }
		public string Email { get; set; }
		public string Phone { get; set; }
        public virtual List<ProgramRegistration> Registrations { get; set; }
		public virtual List<ReadingLog> ReadingLogs { get; set; }
		public virtual List<AwardedPrize> ReceivedPrizes { get; set; }

        [NotMapped]
        public Program CurrentProgram
        {
            get
            {
                return Registrations.OrderByDescending(r => r.RegistrationDate).First().Program;
            }
        }
    }
}
