﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SummerReadingData.Model
{
	public class AwardedPrize
	{
		public int Id { get; set; }
		public int PatronId { get; set; }
		[ForeignKey("PatronId")]
		public Patron Patron { get; set; }		
		public int PrizeId { get; set; }
		[ForeignKey("PrizeId")]
		public Prize Prize { get; set; }
		public DateTime? DateAwarded { get; set; }
	}
}
