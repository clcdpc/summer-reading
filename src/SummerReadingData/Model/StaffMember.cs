﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SummerReadingData.Model
{
	public class StaffMember
	{
		public int Id { get; set; }
        [Required]
		public string Username { get; set; }
        [Required]
		public int OrganizationId { get; set; }
		[ForeignKey("OrganizationId")]
		public virtual Organization Organization { get; set; }
		public virtual List<Role> Roles { get; set; }
	}
}
