﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SummerReadingData.Model
{
    public class ProgramRegistration
    {
        public int ID { get; set; }
        public int PatronId { get; set; }
        public int ProgramId { get; set; }
        public DateTime RegistrationDate { get; set; }

        public virtual Patron Patron { get; set; }
        public virtual Program Program { get; set; }
    }
}
