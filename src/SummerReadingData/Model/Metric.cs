﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SummerReadingData.Model
{
	public class Metric
	{
		public int Id { get; set; }
		public string Description { get; set; }
		public virtual List<Program> Programs { get; set; }
	}
}
