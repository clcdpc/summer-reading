﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SummerReadingData.Model
{
	public class Role
	{
		public int Id { get; set; }
		public string Name { get; set; }
		public string Description { get; set; }
		public List<StaffMember> StaffMembers { get; set; }
	}
}
