﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SummerReadingData.Model
{
	public class Activity
	{
		public int Id { get; set; }
		[Required]
		[Range(1, 100, ErrorMessage="Must be between 1 and 100")]
		public int Value { get; set; }
		[Required]
		public string Description { get; set; }
		public int ProgramId { get; set; }
		public virtual Program Program { get; set; }
	}
}
