﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SummerReadingData.Model
{
	public class PrizeDisplay : Prize
	{
		public int QuantityRemaining { get; set; }
	}
}
