﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Security;

namespace SummerReadingData.Membership
{
	class SummerReadingRoleProvider : RoleProvider 
	{

		public override void AddUsersToRoles(string[] usernames, string[] roleNames)
		{
			throw new NotImplementedException();
		}

		public override string ApplicationName
		{
			get
			{
				throw new NotImplementedException();
			}
			set
			{
				throw new NotImplementedException();
			}
		}

		public override void CreateRole(string roleName)
		{
			throw new NotImplementedException();
		}

		public override bool DeleteRole(string roleName, bool throwOnPopulatedRole)
		{
			throw new NotImplementedException();
		}

		public override string[] FindUsersInRole(string roleName, string usernameToMatch)
		{
			throw new NotImplementedException();
		}

		public override string[] GetAllRoles()
		{
			using (var db = new SummerReadingDb())
			{
				return db.Roles.Select(r => r.Name).ToArray();
			}
		}

		public override string[] GetRolesForUser(string username)
		{
			using (var db = new SummerReadingDb())
			{
				var staffMember = db.StaffMembers.SingleOrDefault(s => s.Username == username);
				return staffMember != null ? staffMember.Roles.Select(r => r.Name).ToArray() : new string[1] { "User" };
			}
		}

		public override string[] GetUsersInRole(string roleName)
		{
			using (var db = new SummerReadingDb())
			{
				return db.StaffMembers.Where(s => s.Roles.Any(r => r.Name == roleName)).Select(s => s.Username).ToArray();
			}
		}

		public override bool IsUserInRole(string username, string roleName)
		{
			using (var db = new SummerReadingDb())
			{
				return db.StaffMembers.Any(s => s.Username == username) ?
					db.StaffMembers.Any(s => s.Username == username && s.Roles.Any(r => r.Name == roleName)) :
					roleName.ToLower() == "user" ? true : false;				
			}
		}

		public override void RemoveUsersFromRoles(string[] usernames, string[] roleNames)
		{
			throw new NotImplementedException();
		}

		public override bool RoleExists(string roleName)
		{
			throw new NotImplementedException();
		}
	}
}
