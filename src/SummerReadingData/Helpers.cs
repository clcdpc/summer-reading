﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;

namespace SummerReadingData
{
	public static class Helpers
	{
		public static MvcHtmlString GenerateEditDeleteDetailsLinks(this HtmlHelper helper, string role, string controller, int id)
		{
			var html = new StringBuilder();
			var routeValues = new RouteValueDictionary(new { id = id });

			if (helper.ViewContext.HttpContext.User.IsInRole(role))
			{
				html.Append(new MvcHtmlString(HtmlHelper.GenerateLink(HttpContext.Current.Request.RequestContext, System.Web.Routing.RouteTable.Routes, "Edit", null, "edit", controller, routeValues, null)));
				html.Append(" | ");
				html.Append(new MvcHtmlString(HtmlHelper.GenerateLink(HttpContext.Current.Request.RequestContext, System.Web.Routing.RouteTable.Routes, "Details", null, "Details", controller, routeValues, null)));
				html.Append(" | ");
				html.Append(new MvcHtmlString(HtmlHelper.GenerateLink(HttpContext.Current.Request.RequestContext, System.Web.Routing.RouteTable.Routes, "Delete", null, "Delete", controller, routeValues, null)));
			}
			else if (helper.ViewContext.HttpContext.User.Identity.IsAuthenticated)
			{
				html.Append(new MvcHtmlString(HtmlHelper.GenerateLink(HttpContext.Current.Request.RequestContext, System.Web.Routing.RouteTable.Routes, "Details", null, "Details", controller, routeValues, null)));
			}
			
			return new MvcHtmlString(html.ToString());
		}
	}
}
